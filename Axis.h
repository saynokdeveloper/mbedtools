#ifndef AXIS_H
#define AXIS_H

#include "mbed.h"
#include "StepMotor.h"
#include "EndStop.h"

#define AXIS_OK 0
#define AXIS_MOVING 1
#define AXIS_ERROR -1
class Axis
{
public:
    Axis(char name);

    //Methods
    void addStepMotor(StepMotor * stepMotor);
    void addEndStop(EndStop * endStop);
    
    int move(float coordinate, float acceleration);
    void stop();
    bool checkEndStop(int type);
    
    void updatePosition();
    void resetPosition(int32_t position);

    //Setters
    void setCoefficient(float distance, int32_t steps);

    //Getters
    float getCurrentPosition();
    bool isMoveAllowed(float newPosition);
    char getName();
    int getState();

    //Callbacks
    void attach(Callback<void(float pos, char name)> cb);

    //IQR
    void IQR_stop(int type);
    void IQR_ready();
    bool IQR_check();

private:
    char name;
    int state;
    
    float coefficient; // 5mm/1600 = 0,003125
    int32_t currentPosition;
    float newPosition;
    
    int attached;

    StepMotor *stepMotorArr[2];
    int stepMotorCount;
    
    EndStop * endStops[4];
    int endStopsCount;

    Callback<void(float pos, char name)> cb;
};

#endif
