#ifndef PinsManager_H
#define PinsManager_H

#include "mbed.h"

class PinsManager
{
public:
    PinsManager(Serial * out,  float rate);

    //Methods
    void addPin(PinName pin, char name[], int activeState = 1);
    void updatePins();
    void printOut();

private:
    Ticker timer;
    Serial * out;
    int pinCount;
    DigitalIn * pinArr[8];
    int pinCurrentStates[8];
    int pinLastStates[8];
    int pinActiveStates[8];
    char name[8][10];
};

#endif
