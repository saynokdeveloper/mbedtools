#ifndef StepMotor_H
#define StepMotor_H

#include "mbed.h"

#define STEP_MOTOR_STOPPED -1
#define STEP_MOTOR_OK 0

#define STEP_DIR_CW 1
#define STEP_DIR_CCW 0


class StepMotor
{
public:
    StepMotor(PinName _pinPull, PinName _pinDir, int direction = STEP_DIR_CCW);
    
    //Methods
    void move(int32_t count, int32_t speed);
    void stop();
    void attach(Callback<void()> cb);
    void attachStep(Callback<bool()> cb_st);
    void clean();
    
    //Setters
    void setDefDirection();
    
    //Getters
    int32_t getCurrentStep();
    
    //IQR
    void IQR_step();

private:
    DigitalOut * pull;
    DigitalOut * dir;
    
    int32_t currentStep;
    int32_t countToMove;
    int defDirection;
    int curDirection;
    
    int pullState;
    Ticker timer;
    int isAttached;
    int stepAttached;
    
    Callback<void()> cb;
    Callback<bool()> cb_st;
};

#endif
