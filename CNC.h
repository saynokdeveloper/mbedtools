#ifndef CNC_H
#define CNC_H

#include "mbed.h"
#include "StepMotor.h"
#include "Axis.h"

#define CNC_OK 0
#define CNC_ERROR_LIMITS -1
#define CNC_ERROR_FLOAT -2

class CNC {
public:

	CNC();

	//
	// Methods
	//

	int move(float x, float y, float z, float speed);
	int moveAxis(char name, float pos, float speed);
	void resetOffset();
	void stop();
	void addAxis(Axis * axis);

	//
	// Setters
	//

	void setMaxPosition(float x, float y, float z);

	//
	// Getters
	//

	float * getCurrentPosition();
	bool checkPosition(float x, float y, float z);
	float getMaxAxisPosition(char name);
	bool checkAxisPosition(char name, float pos);
	bool isReady();
	bool isAxisReady(char name);
	Axis * getAxis(char name);

private:
	Axis * axises[10];
	int axisCount;

	float currentPosition[3];
	float newPosition[3];
	float maxPosition[3];
};

#endif
