#include "HX711.h"

HX711::HX711(PinName pinData, PinName pinSck, int gain) {
	data = new DigitalIn(pinData);
	sck = new DigitalOut(pinSck);
	setGain(gain);

	sck->write(1);
	wait_us(100);
	sck->write(0);
}

//
//methods
//

int32_t HX711::readValue() {
	int32_t buffer = 0;
	if (!isReady())
		return 0;

	for (int i = 0; i < 24; i++) {
		sck->write(1);
		buffer = buffer << 1;
		if (data->read())
			buffer++;
		sck->write(0);

	}
	buffer = buffer >> 1;
	for (int i = 0; i < gain; i++) {
		sck->write(1);
		sck->write(0);
	}
	buffer ^= 0x800000;
	return buffer;
}

int32_t HX711::averageValue(int times) {
	int32_t sum = 0;
	for (int i = 0; i < times; i++) {
		while (!isReady())
			;
		sum += readValue();
	}
	return sum / float(times);
}

void HX711::powerDown() {
	sck->write(0);
	sck->write(1);
}

void HX711::powerUp() {
	sck->write(0);
}

void HX711::tare(int times) {
	int sum = averageValue(times);
	setOffset(sum);
}

float HX711::convert(float val) {
	return (val - float(offset)) / scale;
}

float HX711::measureGram() {
	float weight;
	if (!isReady())
		weight = 0;
	else
		weight = convert(readValue());
	return weight;
}

//
// Setter
//
void HX711::setOffset(int32_t offset) {
	this->offset = offset;
}

void HX711::setScale(float scale) {
	this->scale = scale;
}

void HX711::setGain(int gain) {
	switch (gain) {
	case 128:       // channel A, gain factor 128
		this->gain = 1;
		break;

	case 64:        // channel A, gain factor 64
		this->gain = 3;
		break;

	case 32:        // channel B, gain factor 32
		this->gain = 2;
		break;
	}
	sck->write(0);
}

//
// Getter
//

float HX711::getScale() {
	return scale;
}

int32_t HX711::getOffset() {
	return offset;
}

float HX711::isReady() {
	return data->read() == 0;
}
