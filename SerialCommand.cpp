#include "SerialCommand.h"

SerialCommand::SerialCommand(uint8_t command, Callback<void()> cb,
		uint32_t bufferSize) {
	this->command = command;
	this->cb = cb;
	this->bufferSize = bufferSize;

	if (bufferSize > 0)
		buffer = new uint8_t[bufferSize];
	else
		buffer = new uint8_t[1];
	currentByte = 0;

}

//
// Methods
//

void SerialCommand::clean() {
	currentByte = 0;
}

bool SerialCommand::addByte(uint8_t byte) {
	buffer[currentByte] = byte;
	currentByte++;
	return isReading();
}

void SerialCommand::doCb() {
	cb();
}

//
// Getter
//

bool SerialCommand::checkCommand(uint8_t command) {
	return this->command == command;
}

bool SerialCommand::isReading() {
	return currentByte == bufferSize;
}

bool SerialCommand::isReadable() {
	return bufferSize == 0;
}

int32_t SerialCommand::getBufferSize() {
	return this->bufferSize;
}

uint8_t * SerialCommand::getBuffer() {
	return buffer;
}
