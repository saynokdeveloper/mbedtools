#include "CNC.h"

CNC::CNC() {
}

//
// Methods
//

int CNC::move(float x, float y, float z, float speed) {
	if (!checkPosition(x, y, z))
		return CNC_ERROR_LIMITS;

	float wayX = abs(x - getAxis('X')->getCurrentPosition());
	float wayY = abs(y - getAxis('Y')->getCurrentPosition());
	float wayZ = abs(z - getAxis('Z')->getCurrentPosition());

	float speedX = 0;
	float speedY = 0;
	float speedZ = 0;

	if (wayX >= wayY && wayX >= wayZ) {
		speedX = speed;
		speedY = (speed * wayY) / wayX;
		speedZ = (speed * wayZ) / wayX;
	} else if (wayY >= wayX && wayY >= wayZ) {
		speedX = (speed * wayX) / wayY;
		speedY = speed;
		speedZ = (speed * wayZ) / wayY;

	} else if (wayZ >= wayY && wayZ >= wayX) {
		speedX = (speed * wayX) / wayZ;
		speedY = (speed * wayY) / wayZ;
		speedZ = speed;
	}

	getAxis('X')->stop();
	getAxis('Y')->stop();
	getAxis('Z')->stop();

	int error;

	error = getAxis('X')->move(x - getAxis('X')->getCurrentPosition(), speedX);
	error = getAxis('Y')->move(y - getAxis('Y')->getCurrentPosition(), speedY);
	error = getAxis('Z')->move(z - getAxis('Z')->getCurrentPosition(), speedZ);

	if (error == AXIS_ERROR)
		return CNC_ERROR_FLOAT;

	return CNC_OK;
}

int CNC::moveAxis(char name, float pos, float speed) {
	if (!checkAxisPosition(name, pos))
		return CNC_ERROR_LIMITS;
	getAxis(name)->stop();
	getAxis(name)->move(pos - getAxis(name)->getCurrentPosition(), speed);
	return CNC_OK;
}

void CNC::resetOffset() {
	getAxis('X')->resetPosition(END_STOP_X_TYPE == ES_START ? 0 : CNC_MAX_X_MM);
	getAxis('Y')->resetPosition(END_STOP_Y_TYPE == ES_START ? 0 : CNC_MAX_Y_MM);
	getAxis('Z')->resetPosition(END_STOP_Z_TYPE == ES_START ? 0 : CNC_MAX_Z_MM);
}

void CNC::stop() {
	getAxis('X')->stop();
	getAxis('Y')->stop();
	getAxis('Z')->stop();
}

void CNC::addAxis(Axis * axis) {
	axises[axisCount] = axis;
	axisCount++;
}

//
// Setters
//

void CNC::setMaxPosition(float x, float y, float z) {
	maxPosition[0] = x;
	maxPosition[1] = y;
	maxPosition[2] = z;
}

//
// Getters
//

float * CNC::getCurrentPosition() {
	currentPosition[0] = getAxis('X')->getCurrentPosition();
	currentPosition[1] = getAxis('Y')->getCurrentPosition();
	currentPosition[2] = getAxis('Z')->getCurrentPosition();
	return currentPosition;
}

bool CNC::checkPosition(float x, float y, float z) {
	if (x > maxPosition[0])
		return false;
	if (y > maxPosition[1])
		return false;
	if (z > maxPosition[2])
		return false;
	return true;
}

float CNC::getMaxAxisPosition(char name) {
	if (name == 'X')
		return maxPosition[0];
	if (name == 'Y')
		return maxPosition[1];
	if (name == 'Z')
		return maxPosition[2];
	return 0;
}

bool CNC::checkAxisPosition(char name, float pos) {
	switch (name) {
	case 'X':
		return pos <= maxPosition[0];
	case 'Y':
		return pos <= maxPosition[1];
	case 'Z':
		return pos <= maxPosition[2];
	}
	return true;
}

bool CNC::isReady() {
	if (getAxis('X')->getState() == AXIS_MOVING)
		return false;
	if (getAxis('Y')->getState() == AXIS_MOVING)
		return false;
	if (getAxis('Z')->getState() == AXIS_MOVING)
		return false;
	return true;
}

bool CNC::isAxisReady(char name) {
	return getAxis(name)->getState() == AXIS_OK;
}

Axis * CNC::getAxis(char name) {
	for (int i = 0; i < axisCount; i++)
		if (axises[i]->getName() == name)
			return axises[i];
	return 0;
}
