#include "SerialController.h"

SerialController::SerialController(PinName tx, PinName rx, int baud) :
		Serial(tx, rx, baud) {
	commCount = 0;
	currentCommand = -1;
	this->attach(callback(this, &SerialController::rx_IQR));
}

//
//  Methods
//

void SerialController::putFloat(float val) {
	unsigned char const * p = reinterpret_cast<unsigned char const *>(&val);
	this->putc(p[3]);
	this->putc(p[2]);
	this->putc(p[1]);
	this->putc(p[0]);
}

void SerialController::putInt32(int32_t val) {
	unsigned char const * p = reinterpret_cast<unsigned char const *>(&val);
	this->putc(p[3]);
	this->putc(p[2]);
	this->putc(p[1]);
	this->putc(p[0]);
}

int32_t SerialController::readInt32(int rIndex) {
	int32_t val;
	unsigned char * p = reinterpret_cast<unsigned char *>(&val);
	p[3] = readingBuffer[(rIndex * 4) + 0];
	p[2] = readingBuffer[(rIndex * 4) + 1];
	p[1] = readingBuffer[(rIndex * 4) + 2];
	p[0] = readingBuffer[(rIndex * 4) + 3];
	return val;
}

float SerialController::readFloat(int rIndex) {
	float val;
	unsigned char * p = reinterpret_cast<unsigned char *>(&val);
	p[3] = readingBuffer[(rIndex * 4) + 0];
	p[2] = readingBuffer[(rIndex * 4) + 1];
	p[1] = readingBuffer[(rIndex * 4) + 2];
	p[0] = readingBuffer[(rIndex * 4) + 3];
	return val;
}

void SerialController::addRespond(uint8_t command, Callback<void()> cb,
		uint32_t bufferSize) {
	cbCommand[commCount] = new SerialCommand(command, cb, bufferSize);
	commCount++;
}

void SerialController::checkCommand(uint8_t command) {
	for (int i = 0; i < commCount; i++)
		if (cbCommand[i]->checkCommand(command)) {
			currentCommand = i;
			isAllowed = cbCommand[currentCommand]->isReadable();
		}
}

void SerialController::work() {
	if (rxBufferSize > 0) {
		if (currentCommand >= 0)
			isAllowed = cbCommand[currentCommand]->addByte(readBuffer());
		else
			checkCommand(readBuffer());
	}

	if (currentCommand >= 0 && isAllowed) {
		readingBuffer = cbCommand[currentCommand]->getBuffer();
		cbCommand[currentCommand]->doCb();
		cbCommand[currentCommand]->clean();
		currentCommand = -1;
	}
}

//
// 	Getter
//

uint8_t * SerialController::getBuffer() {
	return readingBuffer;
}

uint8_t SerialController::readBuffer() {
	uint8_t data = rxBuffer[0];
	for (int i = 0; i < rxBufferSize; i++)
		rxBuffer[i] = rxBuffer[i + 1];

	rxBufferSize--;
	return data;
}

//
//	IQR
//

void SerialController::rx_IQR() {

	uint8_t value = this->getc();
	rxBuffer[rxBufferSize] = value;
	rxBufferSize++;
}
