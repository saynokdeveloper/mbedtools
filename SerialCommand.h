#ifndef SerialCommand_H
#define SerialCommand_H

#include "mbed.h"

class SerialCommand
{
public:
	SerialCommand(uint8_t command, Callback<void()> cb, uint32_t bufferSize);

    //
    // 	Methods
    //

    void clean();
    bool addByte(uint8_t byte);
    void doCb();

    //
    // 	Getter
    //

    bool checkCommand(uint8_t command);
    bool isReading();
    bool isReadable();
    int32_t getBufferSize();
    uint8_t * getBuffer();

private:
    Callback <void()> cb;
    uint8_t command;
    uint8_t * buffer;
    int32_t bufferSize;
    int32_t currentByte;
};

#endif

