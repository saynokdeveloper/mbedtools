#include "Axis.h"

Axis::Axis(char name) {
	this->name = name;
	attached = 0;
	stepMotorCount = 0;
	endStopsCount = 0;
	state = AXIS_OK;
}

//
// Methods
//

void Axis::addStepMotor(StepMotor * stepmotor) {
	stepMotorArr[stepMotorCount] = stepmotor;
	stepMotorArr[stepMotorCount]->attach(callback(this, &Axis::IQR_ready));
	stepMotorArr[stepMotorCount]->attachStep(callback(this, &Axis::IQR_check));
	stepMotorCount++;
}

void Axis::addEndStop(EndStop * endStop) {
	endStops[endStopsCount] = endStop;
	endStopsCount++;
}

int Axis::move(float newPosition, float speed) {
	if (!isMoveAllowed(newPosition) || newPosition == 0
			|| (abs(newPosition) < coefficient) || (abs(speed) < coefficient))
		return AXIS_ERROR;

	this->newPosition = newPosition;
	for (int i = 0; i < stepMotorCount; i++)
		stepMotorArr[i]->move(int32_t(newPosition / coefficient),
				int32_t(speed / coefficient));
	state = AXIS_MOVING;
	return AXIS_MOVING;
}

void Axis::attach(Callback<void(float pos, char name)> cb) {
	this->cb = cb;
	attached = 1;
}

void Axis::stop() {
	for (int i = 0; i < stepMotorCount; i++)
		stepMotorArr[i]->stop();
}

void Axis::updatePosition() {
	currentPosition += stepMotorArr[0]->getCurrentStep();

}

void Axis::resetPosition(int32_t position) {
	currentPosition = position / coefficient;
}

bool Axis::isMoveAllowed(float newPosition) {
	if (newPosition > 0)
		if (checkEndStop(ES_END))
			return false;

	if (newPosition < 0)
		if (checkEndStop(ES_START))
			return false;

	return true;
}

//
// Setters
//

void Axis::setCoefficient(float distance, int32_t steps) {
	coefficient = distance / steps;
}

//
// Getters
//

char Axis::getName() {
	return name;
}

int Axis::getState() {
	return state;
}

float Axis::getCurrentPosition() {
	return (currentPosition + stepMotorArr[0]->getCurrentStep()) * coefficient;
}

bool Axis::IQR_check() {
	if (!isMoveAllowed(newPosition)) {
		stop();
		return false;
	}
	return true;
}

bool Axis::checkEndStop(int type) {
	for (int i = 0; i < endStopsCount; i++)
		if (endStops[i]->isActive(type))
			return true;
	return false;
}

//
// IQR
//

void Axis::IQR_ready() {
	updatePosition();
	for (int i = 0; i < stepMotorCount; i++)
		stepMotorArr[i]->clean();
	state = AXIS_OK;
	if (attached)
		cb(getCurrentPosition(), getName());
}
