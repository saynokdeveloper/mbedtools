#ifndef EndStop_H
#define EndStop_H

#include "mbed.h"

#define ES_START 0
#define ES_END 1

class EndStop
{

public:
    EndStop(PinName pin, int type, int activeState); 
    //getter
    bool isActive(int type);
    int getType();
    
private:
    int type;
    int activeState;
    PinName pin;
};

#endif
