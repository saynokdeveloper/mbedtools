#include "StepMotor.h"

StepMotor::StepMotor(PinName pinPull, PinName pinDir, int defDirection)
{
    pull = new DigitalOut(pinPull);
    dir = new DigitalOut(pinDir);

    isAttached = 0;
    stepAttached = 0;
    this->defDirection = defDirection;
    clean();
}

//
// Methods
//

void StepMotor::stop()
{
    timer.detach();
    if(isAttached == 1)
        cb();
}

void StepMotor::move(int32_t countToMove, int32_t speed)
{
    clean();

    this->countToMove = countToMove;
    curDirection = countToMove > 0 ? defDirection : !defDirection;
    dir->write(curDirection);
    this->timer.attach(callback(this, &StepMotor::IQR_step), (1.0f/speed)/2.0f);
    // print the timer belonging axis name, like "timer X is attched," and "detached"...
    // can we add a attribute for the timer class, then add the name for the timer instance.

}

void StepMotor::clean()
{
    pullState = 0;
    currentStep = 0;
    countToMove = 0;
}

//
// Getters
//

int32_t StepMotor::getCurrentStep()
{
    return currentStep * (curDirection == defDirection ? 1 : -1);
}

//
// Callback
//

void StepMotor::attach(Callback<void()> cb)
{
    isAttached = 1;
    this->cb = cb;
}

void StepMotor::attachStep(Callback<bool()> cb_st)
{
    stepAttached = 1;
    this->cb_st = cb_st;
}

//
// IQR
//

void StepMotor::IQR_step()
{
    if(stepAttached)
        if(!cb_st())
            return;
    if(pullState == 1) {
        if(currentStep == abs(countToMove)) {
            stop();
            return;
        }
        currentStep++;
    }
    pull->write(pullState);
    pullState = !pullState;
}
