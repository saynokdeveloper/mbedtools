#include "PID.h"

PID::PID() {

}

//
//	Methods
//

float PID::calculate(float newValue) {
	error = targetValue - newValue;

	// Calculate P
	p = kp * error;

	// Calculate I
	//float errorAverage = ((weightMax - weightMin) / 2);
	i += error * ki;

	// Calculate D
	d = kd * ((error - previousError) / rate);

	// Calculate PID
	value = p + i + d;
	previousError = error;
	return value;
}

//
//	Setter
//

void PID::setKP(float kp) {
	this->kp = kp;
}

void PID::setKI(float ki) {
	this->ki = ki;
}

void PID::setKD(float kd) {
	this->kd = kd;
}

void PID::setRate(float rate) {
	this->rate = rate;
}

void PID::setTargetValue(float targetValue) {
	this->targetValue = targetValue;
}

