#ifndef HX711_H
#define HX711_H

#include "mbed.h"

class HX711 {
public:
	HX711(PinName pinData, PinName pinSck, int gain = 128);

	//
	//	Methods
	//

	int32_t readValue();
	int32_t averageValue(int times);
	void powerDown();
	void powerUp();
	void tare(int times = 10);
	float convert(float val);
	float measureGram();

	//
	// 	Setter
	//

	void setOffset(int32_t offset);
	void setScale(float scale);
	void setGain(int gain);

	//
	// 	Getter
	//

	float getScale();
	int32_t getOffset();
	float isReady();

private:
	DigitalOut *sck;
	DigitalIn *data;

	int32_t offset = 0;
	float scale = 0;
	int gain = 0;

	bool enable;
};

#endif

