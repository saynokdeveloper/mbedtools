#include "PinsManager.h"

PinsManager::PinsManager(Serial * out, float rate) {
	pinCount = 0;
	this->out = out;
	timer.attach(callback(this, &PinsManager::updatePins), rate);
}

//
// Methods
//

void PinsManager::addPin(PinName pin, char name[], int activeState) {
	for (int i = 0; i < sizeof(name); i++)
		this->name[pinCount][i] = name[i];
	pinArr[pinCount] = new DigitalIn(pin);
	pinLastStates[pinCount] = pinArr[pinCount]->read();
	pinActiveStates[pinCount] = activeState;
	pinCount++;
}

void PinsManager::updatePins() {
	for (int i = 0; i < pinCount; i++)
		pinCurrentStates[i] = pinArr[i]->read();
	printOut();
}

void PinsManager::printOut() {
	for (int i = 0; i < pinCount; i++) {

		if (pinCurrentStates[i] != pinLastStates[i]) {
			out->printf(
					pinCurrentStates[i] == pinActiveStates[i] ?
							"%s, pushed!\n" : "%s, released!\n", name[i]);
		}

		// 1. check if state is changed 0-1 or 1-0
		// 2. check if state = ACTIVE_CONFIG , then print "pushed"
	}
	for (int i = 0; i < pinCount; i++)
		pinLastStates[i] = pinCurrentStates[i];
}
