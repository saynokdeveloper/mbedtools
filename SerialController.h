#ifndef SerialController_H
#define SerialController_H

#include "SerialCommand.h"

class SerialController: public Serial {
public:
	SerialController(PinName tx, PinName rx, int baud);

	//
	//  Methods
	//

	void putFloat(float val);
	void putInt32(int32_t val);
	int32_t readInt32(int rIndex = 0);
	float readFloat(int rIndex = 0);
	void addRespond(uint8_t command, Callback<void()> cb, uint32_t bufferSize =
			0);
	void checkCommand(uint8_t command);
	void work();

	//
	// 	Getter
	//

	uint8_t * getBuffer();
	uint8_t readBuffer();

	//
	//	IQR
	//

	void rx_IQR();

private:
	SerialCommand * cbCommand[50];
	int currentCommand;
	int commCount;

	bool isAllowed;

	uint8_t * readingBuffer;

	uint8_t rxBuffer[256];
	int rxBufferSize;

};

#endif

