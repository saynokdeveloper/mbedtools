#ifndef LIBS_PID_H_
#define LIBS_PID_H_

class PID {
public:
	PID();

	//	Methods
	float calculate(float newValue);

	//	Setter
	void setKP(float kp);
	void setKI(float ki);
	void setKD(float kd);
	void setRate(float rate);
	void setTargetValue(float targetValue);

	//	Getter
	float getError(){
		return error;
	}

private:
	float p;
	float i;
	float d;
	float kp;
	float ki;
	float kd;
	float error;
	float previousError;
	float value;
	float rate;
	float targetValue;
};

#endif /* LIBS_PID_H_ */
